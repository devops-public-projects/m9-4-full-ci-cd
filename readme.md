
# Complete CI/CD Pipeline
Complete the CI/CD Pipeline (Docker-Compose, Dynamic
versioning)

## Technologies Used
- AWS
- Jenkins
- Docker
- Linux
- Git
- Java
- Maven
- Docker Hub

## Project Description
- CI step: Increment version
- CI step: Build artifact for Java Maven application
- CI step: Build and push Docker image to Docker Hub
- CD step: Deploy new application version with Docker Compose
- CD step: Commit the version update

## Prerequisites
- AWS
- Jenkins
- Docker
- Linux
- Git
- Java
- Maven
- Docker Hub

## Guide Steps
### Adding Dynamic Versioning
- Our previous Jenkinsfile has an environment section with our IMAGE_NAME which we will remove since we want to set this dynamically and not hard code it. We will then add a new build stage for "Increment" as the first part of the pipeline with the contents below.
```groovy
 echo 'Incrementing App Version'
 sh 'mvn build-helper:parse-version versions:set \
 -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
 versions:commit'
 def matcher = readFile('pom.xml') =~ '<version>(.*)</version>'
 def version = matcher[0][1]
 env.IMAGE_NAME = "theinstinct/demo-app:$version-$BUILD_NUMBER"
```
- This change also needs to be saved so after we deploy we want Jenkins to check this code change back in **and** we don't want to trigger a build loop so we need to make sure GitLab doesn't trigger Jenkins to start a new build again.
**Check-in Version Update Stage**
```groovy
script {
	 withCredentials([usernamePassword(credentialsId:  'gitlab-credentials', passwordVariable:  'PASS', usernameVariable:  'USER')]) {
		 sh 'git config --global user.email "jenkins@example.com"'
		 sh 'git config --global user.name "jenkins"'

		 sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/devops-public-projects/aws-temp-app-code.git"
		 sh 'git add .'
		 sh 'git commit -m "Jenkins: version bump"'
		 sh "git push origin HEAD:${BRANCH_NAME}"
 }
```
